
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//package quadradooito;

/**
 *
 * @author felipe
 */
public class Rosto {
    public Image imagem;
    public TriangulatedImage t;
    public Graphics2D g2dt;

    public Rosto(String filename) {
        this.imagem =  new javax.swing.ImageIcon(filename).getImage();
        this.t = new TriangulatedImage();
        this.t.bi = new BufferedImage(150, 150, BufferedImage.TYPE_INT_RGB);
        
        this.g2dt = this.t.bi.createGraphics();
    }
    
    public void draw() {
        this.g2dt.drawImage(this.imagem, 0, 0, null);
    }
    
    public void addPontos(Point2D.Double[] pontos) {
        this.t.tPoints = new Point2D[9];
        
        for (int i = 0; i < 9; i++)
            this.t.tPoints[i] = pontos[i];
        
        this.t.triangles = new int[8][3];
        
        for (int i = 0; i < 7; i++) {
            this.t.triangles[i][0] = i;
            this.t.triangles[i][1] = i+1;
            this.t.triangles[i][2] = 8;
        }
        
        this.t.triangles[7][0] = 7;
        this.t.triangles[7][1] = 0;
        this.t.triangles[7][2] = 8;
    }
    
}
