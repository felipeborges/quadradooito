//package quadradooito;

/**
 * O Java não tem (a mão) uma estrutura de Tupla que seja simples como 
 * precisamos.
 * 
 * A estrutura abaixo representa um ponto no plano 2D. As coordenadas desse 
 * ponto podem ser acessadas com Ponto.x e Ponto.y.
 * 
 * @author felipe
 */
public class Ponto {
    public  int x;
    public  int y;
    
    public Ponto(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
