//package quadradooito;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class QuadradoOito extends TimerTask{    
    private Caminho caminho;
    
    public ArrayList trajX;
    public ArrayList trajY;
    
    public static double steps = 500;
    
    private BufferedImage mix;
    
    private Rosto bran;
    private Rosto robb;
    
    private double alpha;
    private double deltaAlpha;
    
    private int width;
    private int height;
    
    private TriangulatedImage t1;
    private TriangulatedImage t2;
    
    private BufferedImageDrawer buffid;

    Rosto[] rostos;    
    int rostoAtual;
    
    public Ponto posAtual;
    
    private int count;
    
    /* Transformacoes */
    private double[] initialMatrix;
    private double[] finalMatrix;
    
    private AffineTransform initialTransform, intermediateTransform, finalTransform;

    private double transformIter;
        
    public static double[] convexCombination(double[] a, double[] b, double alpha) {
        double[] result = new double[a.length];

        for (int i=0; i<result.length; i++) {
            result[i] = (1-alpha)*a[i] + alpha*b[i];
        }

        return(result);
    }
    
    public QuadradoOito(BufferedImageDrawer bid) {
        this.buffid = bid;
        
        rostos = new Rosto[17];
                
        alpha = 0;
        steps = 100;
        deltaAlpha = 1.0/steps;
        
        Rosto bran = new Rosto("got/BranStark.png");
        bran.draw();
        
        Point2D.Double[] branPontos = new Point2D.Double[9];
        branPontos[0] = new Point2D.Double(0, 0);
        branPontos[1] = new Point2D.Double(229, 0);
        
        branPontos[2] = new Point2D.Double(91, 91);
        branPontos[3] = new Point2D.Double(149, 92);
        
        branPontos[4] = new Point2D.Double(119, 131);
        
        branPontos[5] = new Point2D.Double(99, 158);
        branPontos[6] = new Point2D.Double(145, 153);
        
        branPontos[7] = new Point2D.Double(0, 226);
        branPontos[8] = new Point2D.Double(229, 224);
        bran.addPontos(branPontos);
        
        rostos[0] = bran;
        rostoAtual = 0;
        
        Rosto robb = new Rosto("got/RobbStark.png");
        robb.draw();
        
        Point2D.Double[] robbPontos = new Point2D.Double[9];
        robbPontos[0] = new Point2D.Double(0, 0);
        robbPontos[1] = new Point2D.Double(229, 0);
        
        robbPontos[2] = new Point2D.Double(91, 91);
        robbPontos[3] = new Point2D.Double(149, 92);
        
        robbPontos[4] = new Point2D.Double(119, 131);
        
        robbPontos[5] = new Point2D.Double(99, 158);
        robbPontos[6] = new Point2D.Double(145, 153);
        
        robbPontos[7] = new Point2D.Double(0, 226);
        robbPontos[8] = new Point2D.Double(229, 224);
        robb.addPontos(robbPontos);
        
        rostos[1] = robb;
        
        Rosto eddard = new Rosto("got/EddardStark.png");
        eddard.draw();
        
        Point2D.Double[] eddardPontos = new Point2D.Double[9];
        eddardPontos[0] = new Point2D.Double(0, 0);
        eddardPontos[1] = new Point2D.Double(229, 0);
        
        eddardPontos[2] = new Point2D.Double(91, 91);
        eddardPontos[3] = new Point2D.Double(149, 92);
        
        eddardPontos[4] = new Point2D.Double(119, 131);
        
        eddardPontos[5] = new Point2D.Double(99, 158);
        eddardPontos[6] = new Point2D.Double(145, 153);
        
        eddardPontos[7] = new Point2D.Double(0, 226);
        eddardPontos[8] = new Point2D.Double(229, 224);
        eddard.addPontos(eddardPontos);
        
        rostos[2] = eddard;
        
        Rosto sansa = new Rosto("got/SansaStark.png");
        sansa.draw();
        
        Point2D.Double[] sansaPontos = new Point2D.Double[9];
        sansaPontos[0] = new Point2D.Double(0, 0);
        sansaPontos[1] = new Point2D.Double(229, 0);
        
        sansaPontos[2] = new Point2D.Double(91, 91);
        sansaPontos[3] = new Point2D.Double(149, 92);
        
        sansaPontos[4] = new Point2D.Double(119, 131);
        
        sansaPontos[5] = new Point2D.Double(99, 158);
        sansaPontos[6] = new Point2D.Double(145, 153);
        
        sansaPontos[7] = new Point2D.Double(0, 226);
        sansaPontos[8] = new Point2D.Double(229, 224);
        sansa.addPontos(sansaPontos);
        
        rostos[3] = sansa;

        Rosto catelyn = new Rosto("got/CatelynStark.png");
        catelyn.draw();
        
        Point2D.Double[] catelynPontos = new Point2D.Double[9];
        catelynPontos[0] = new Point2D.Double(0, 0);
        catelynPontos[1] = new Point2D.Double(229, 0);
        
        catelynPontos[2] = new Point2D.Double(91, 91);
        catelynPontos[3] = new Point2D.Double(149, 92);
        
        catelynPontos[4] = new Point2D.Double(119, 131);
        
        catelynPontos[5] = new Point2D.Double(99, 158);
        catelynPontos[6] = new Point2D.Double(145, 153);
        
        catelynPontos[7] = new Point2D.Double(0, 226);
        catelynPontos[8] = new Point2D.Double(229, 224);
        catelyn.addPontos(catelynPontos);
        
        rostos[4] = catelyn;

        /* Lannisters */
        Rosto joffrey = new Rosto("got/JoffreyLannister.png");
        joffrey.draw();
        
        Point2D.Double[] joffreyPontos = new Point2D.Double[9];
        joffreyPontos[0] = new Point2D.Double(0, 0);
        joffreyPontos[1] = new Point2D.Double(229, 0);
        
        joffreyPontos[2] = new Point2D.Double(91, 91);
        joffreyPontos[3] = new Point2D.Double(149, 92);
        
        joffreyPontos[4] = new Point2D.Double(119, 131);
        
        joffreyPontos[5] = new Point2D.Double(99, 158);
        joffreyPontos[6] = new Point2D.Double(145, 153);
        
        joffreyPontos[7] = new Point2D.Double(0, 226);
        joffreyPontos[8] = new Point2D.Double(229, 224);
        joffrey.addPontos(joffreyPontos);
        
        rostos[5] = joffrey;

        Rosto jaime = new Rosto("got/JaimeLannister.png");
        jaime.draw();
        
        Point2D.Double[] jaimePontos = new Point2D.Double[9];
        jaimePontos[0] = new Point2D.Double(0, 0);
        jaimePontos[1] = new Point2D.Double(229, 0);
        
        jaimePontos[2] = new Point2D.Double(91, 91);
        jaimePontos[3] = new Point2D.Double(149, 92);
        
        jaimePontos[4] = new Point2D.Double(119, 131);
        
        jaimePontos[5] = new Point2D.Double(99, 158);
        jaimePontos[6] = new Point2D.Double(145, 153);
        
        jaimePontos[7] = new Point2D.Double(0, 226);
        jaimePontos[8] = new Point2D.Double(229, 224);
        jaime.addPontos(jaimePontos);
        
        rostos[6] = jaime;
        
        Rosto cersei = new Rosto("got/CerseiLannister.png");
        cersei.draw();
        
        Point2D.Double[] cerseiPontos = new Point2D.Double[9];
        cerseiPontos[0] = new Point2D.Double(0, 0);
        cerseiPontos[1] = new Point2D.Double(229, 0);
        
        cerseiPontos[2] = new Point2D.Double(91, 91);
        cerseiPontos[3] = new Point2D.Double(149, 92);
        
        cerseiPontos[4] = new Point2D.Double(119, 131);
        
        cerseiPontos[5] = new Point2D.Double(99, 158);
        cerseiPontos[6] = new Point2D.Double(145, 153);
        
        cerseiPontos[7] = new Point2D.Double(0, 226);
        cerseiPontos[8] = new Point2D.Double(229, 224);
        cersei.addPontos(cerseiPontos);
        
        rostos[7] = cersei;
        
        Rosto tyrion = new Rosto("got/TyrionLannister.png");
        tyrion.draw();
        
        Point2D.Double[] tyrionPontos = new Point2D.Double[9];
        tyrionPontos[0] = new Point2D.Double(0, 0);
        tyrionPontos[1] = new Point2D.Double(229, 0);
        
        tyrionPontos[2] = new Point2D.Double(91, 91);
        tyrionPontos[3] = new Point2D.Double(149, 92);
        
        tyrionPontos[4] = new Point2D.Double(119, 131);
        
        tyrionPontos[5] = new Point2D.Double(99, 158);
        tyrionPontos[6] = new Point2D.Double(145, 153);
        
        tyrionPontos[7] = new Point2D.Double(0, 226);
        tyrionPontos[8] = new Point2D.Double(229, 224);
        tyrion.addPontos(tyrionPontos);
        
        rostos[8] = tyrion;
        
        Rosto daenerys = new Rosto("got/DaenerysTargaryen.png");
        daenerys.draw();
        
        Point2D.Double[] daenerysPontos = new Point2D.Double[9];
        daenerysPontos[0] = new Point2D.Double(0, 0);
        daenerysPontos[1] = new Point2D.Double(229, 0);
        
        daenerysPontos[2] = new Point2D.Double(91, 91);
        daenerysPontos[3] = new Point2D.Double(149, 92);
        
        daenerysPontos[4] = new Point2D.Double(119, 131);
        
        daenerysPontos[5] = new Point2D.Double(99, 158);
        daenerysPontos[6] = new Point2D.Double(145, 153);
        
        daenerysPontos[7] = new Point2D.Double(0, 226);
        daenerysPontos[8] = new Point2D.Double(229, 224);
        daenerys.addPontos(daenerysPontos);
        
        rostos[10] = daenerys;
        
        Rosto sandor = new Rosto("got/SandorClegane.png");
        sandor.draw();
        
        Point2D.Double[] sandorPontos = new Point2D.Double[9];
        sandorPontos[0] = new Point2D.Double(0, 0);
        sandorPontos[1] = new Point2D.Double(229, 0);
        
        sandorPontos[2] = new Point2D.Double(91, 91);
        sandorPontos[3] = new Point2D.Double(149, 92);
        
        sandorPontos[4] = new Point2D.Double(119, 131);
        
        sandorPontos[5] = new Point2D.Double(99, 158);
        sandorPontos[6] = new Point2D.Double(145, 153);
        
        sandorPontos[7] = new Point2D.Double(0, 226);
        sandorPontos[8] = new Point2D.Double(229, 224);
        sandor.addPontos(sandorPontos);
        
        rostos[9] = sandor;

        Rosto melisandre = new Rosto("got/Melisandre.png");
        melisandre.draw();
        
        Point2D.Double[] melisandrePontos = new Point2D.Double[9];
        melisandrePontos[0] = new Point2D.Double(0, 0);
        melisandrePontos[1] = new Point2D.Double(229, 0);
        
        melisandrePontos[2] = new Point2D.Double(91, 91);
        melisandrePontos[3] = new Point2D.Double(149, 92);
        
        melisandrePontos[4] = new Point2D.Double(119, 131);
        
        melisandrePontos[5] = new Point2D.Double(99, 158);
        melisandrePontos[6] = new Point2D.Double(145, 153);
        
        melisandrePontos[7] = new Point2D.Double(0, 226);
        melisandrePontos[8] = new Point2D.Double(229, 224);
        melisandre.addPontos(melisandrePontos);
        
        rostos[11] = melisandre;

        Rosto ramsay = new Rosto("got/RamsayBolton.png");
        ramsay.draw();
        
        Point2D.Double[] ramsayPontos = new Point2D.Double[9];
        ramsayPontos[0] = new Point2D.Double(0, 0);
        ramsayPontos[1] = new Point2D.Double(229, 0);
        
        ramsayPontos[2] = new Point2D.Double(91, 91);
        ramsayPontos[3] = new Point2D.Double(149, 92);
        
        ramsayPontos[4] = new Point2D.Double(119, 131);
        
        ramsayPontos[5] = new Point2D.Double(99, 158);
        ramsayPontos[6] = new Point2D.Double(145, 153);
        
        ramsayPontos[7] = new Point2D.Double(0, 226);
        ramsayPontos[8] = new Point2D.Double(229, 224);
        ramsay.addPontos(ramsayPontos);
        
        rostos[12] = ramsay;

        Rosto arya = new Rosto("got/AryaStark.png");
        arya.draw();
        
        Point2D.Double[] aryaPontos = new Point2D.Double[9];
        aryaPontos[0] = new Point2D.Double(0, 0);
        aryaPontos[1] = new Point2D.Double(229, 0);
        
        aryaPontos[2] = new Point2D.Double(91, 91);
        aryaPontos[3] = new Point2D.Double(149, 92);
        
        aryaPontos[4] = new Point2D.Double(119, 131);
        
        aryaPontos[5] = new Point2D.Double(99, 158);
        aryaPontos[6] = new Point2D.Double(145, 153);
        
        aryaPontos[7] = new Point2D.Double(0, 226);
        aryaPontos[8] = new Point2D.Double(229, 224);
        arya.addPontos(aryaPontos);
        
        rostos[13] = arya;
        
        Rosto jon = new Rosto("got/JonSnow.png");
        jon.draw();
        
        Point2D.Double[] jonPontos = new Point2D.Double[9];
        jonPontos[0] = new Point2D.Double(0, 0);
        jonPontos[1] = new Point2D.Double(229, 0);
        
        jonPontos[2] = new Point2D.Double(91, 91);
        jonPontos[3] = new Point2D.Double(149, 92);
        
        jonPontos[4] = new Point2D.Double(119, 131);
        
        jonPontos[5] = new Point2D.Double(99, 158);
        jonPontos[6] = new Point2D.Double(145, 153);
        
        jonPontos[7] = new Point2D.Double(0, 226);
        jonPontos[8] = new Point2D.Double(229, 224);
        jon.addPontos(jonPontos);
        
        rostos[14] = jon;

        Rosto theon = new Rosto("got/TheonGreyjoy.png");
        theon.draw();
        
        Point2D.Double[] theonPontos = new Point2D.Double[9];
        theonPontos[0] = new Point2D.Double(0, 0);
        theonPontos[1] = new Point2D.Double(229, 0);
        
        theonPontos[2] = new Point2D.Double(91, 91);
        theonPontos[3] = new Point2D.Double(149, 92);
        
        theonPontos[4] = new Point2D.Double(119, 131);
        
        theonPontos[5] = new Point2D.Double(99, 158);
        theonPontos[6] = new Point2D.Double(145, 153);
        
        theonPontos[7] = new Point2D.Double(0, 226);
        theonPontos[8] = new Point2D.Double(229, 224);
        theon.addPontos(theonPontos);
        
        rostos[15] = theon;

        Rosto ygritte = new Rosto("got/Ygritte.png");
        ygritte.draw();
        
        Point2D.Double[] ygrittePontos = new Point2D.Double[9];
        ygrittePontos[0] = new Point2D.Double(0, 0);
        ygrittePontos[1] = new Point2D.Double(229, 0);
        
        ygrittePontos[2] = new Point2D.Double(91, 91);
        ygrittePontos[3] = new Point2D.Double(149, 92);
        
        ygrittePontos[4] = new Point2D.Double(119, 131);
        
        ygrittePontos[5] = new Point2D.Double(99, 158);
        ygrittePontos[6] = new Point2D.Double(145, 153);
        
        ygrittePontos[7] = new Point2D.Double(0, 226);
        ygrittePontos[8] = new Point2D.Double(229, 224);
        ygritte.addPontos(ygrittePontos);
        
        rostos[16] = ygritte;


        bran.t.triangles = robb.t.triangles;
        eddard.t.triangles = robb.t.triangles;
        sansa.t.triangles = robb.t.triangles;
        catelyn.t.triangles = robb.t.triangles;
        arya.t.triangles = robb.t.triangles;

        jaime.t.triangles = robb.t.triangles;
        cersei.t.triangles = jaime.t.triangles;
        tyrion.t.triangles = jaime.t.triangles;
        
        sandor.t.triangles = jaime.t.triangles;
        daenerys.t.triangles = jaime.t.triangles;
        melisandre.t.triangles = jaime.t.triangles;
        ramsay.t.triangles = jaime.t.triangles;
        jon.t.triangles = jaime.t.triangles;
        theon.t.triangles = jaime.t.triangles;
        ygritte.t.triangles = jaime.t.triangles;
        
        this.transformIter = 0;
        this.count = 0;
    }
    
    private void setPosAtual(Ponto p) {
        this.posAtual = p;
    }
    
    private void moverPara(Ponto p) {
        this.initialMatrix = new double[6];
        this.finalMatrix = new double[6];
        
        this.initialTransform = new AffineTransform();
        this.finalTransform = new AffineTransform();
        
        this.initialTransform.getMatrix(this.initialMatrix);

        this.finalTransform.translate(p.x - this.posAtual.x, p.y - this.posAtual.y);
        this.finalTransform.getMatrix(this.finalMatrix);
    }
        
    private void desenharCirculo(int x0, int y0, int radius) {
        this.caminho = new Caminho();
        ArrayList<ArrayList> vetX = new ArrayList();
        ArrayList<ArrayList> vetY = new ArrayList();
              
        int f = 1 - radius;
        int ddF_x = 1;
        int ddF_y = -2 * radius;
        int x = 0;
        int y = radius;
        int j;
        
        for (int i = 0; i < 16; i++) {
            vetX.add(new ArrayList<Integer>());
            vetY.add(new ArrayList<Integer>());
        }
        
        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                while (x < y) {
                    if (f >= 0) {
                        y--;
                        ddF_y += 2;
                        f+= ddF_y;
                    }
                    x++;
                    ddF_x += 2;
                    f += ddF_x;
                    
                    vetX.get(0).add(x0 + x); vetY.get(0).add(y0 -y);
                    vetX.get(1).add(x0 + y); vetY.get(1).add(y0 -x);
                    vetX.get(2).add(x0 + y); vetY.get(2).add(y0 +x);
                    vetX.get(3).add(x0 + x); vetY.get(3).add(y0 +y);
                    
                    vetX.get(12).add(x0-x);
                    vetY.get(12).add(y0+y);
 
                    vetX.get(13).add(x0-y);
                    vetY.get(13).add(y0+x);
 
                    vetX.get(14).add(x0-y);
                    vetY.get(14).add(y0-x);
 
                    vetX.get(15).add(x0-x);
                    vetY.get(15).add(y0-y);
                }
            } else {
                f = 1 - radius;
                ddF_x = 1;
                ddF_y = -2 * radius;
                x = 0;
                y = radius;            
                y0 += 2*radius;
                                   
                while(x < y) {
                    if(f >= 0)
                    {
                      y--;
                      ddF_y += 2;
                      f += ddF_y;
                    }
                    x++;
                    ddF_x += 2;
                    f += ddF_x;
                    
                    vetX.get(4).add(x0 - x); vetY.get(4).add(y0 - y);
                    vetX.get(5).add(x0 - y); vetY.get(5).add(y0 - x);
                    vetX.get(6).add(x0 - y); vetY.get(6).add(y0 + x);
                    vetX.get(7).add(x0 - x); vetY.get(7).add(y0 + y);
                    vetX.get(8).add(x0 + x); vetY.get(8).add(y0 + y);
                    vetX.get(9).add(x0 + y); vetY.get(9).add(y0 + x);
                    vetX.get(10).add(x0 + y); vetY.get(10).add(y0 - x);
                    vetX.get(11).add(x0 + x); vetY.get(11).add(y0 - y);
                }
            }
        
        }
        for(int i=1; i<16; i+=2){
            ArrayList<Integer> auxiliarX = new ArrayList<>();
            ArrayList<Integer> auxiliarY = new ArrayList<>();
            
            for(j=vetX.get(i).size()-1; j>=0;j--){
                auxiliarX.add((Integer) vetX.get(i).get(j));
                auxiliarY.add((Integer) vetY.get(i).get(j));
            }
            
            for(j=0; j<auxiliarX.size(); j++) {
                vetX.get(i).set(j, auxiliarX.get(j));
                vetY.get(i).set(j, auxiliarY.get(j));
            }
            
            auxiliarX.clear();
            auxiliarY.clear();
        }
        
        this.trajX = new ArrayList<Integer>();
        this.trajY = new ArrayList<Integer>();
        
        for (int k = 0; k < 16; k++) {
            this.trajX.addAll(vetX.get(k));
            this.trajY.addAll(vetY.get(k));
        }
    }

    public void run() {
        //this.intermediateTransform = new AffineTransform(
          //      convexCombination(this.initialMatrix, this.finalMatrix, this.transformIter/steps));
        
        if (alpha >= 0 && alpha<= 1) {
            try {
                mix = rostos[rostoAtual].t.mixWith(rostos[rostoAtual+1].t, alpha);
            } catch (ArrayIndexOutOfBoundsException e) {
            }

            //buffid.g2dbi.drawImage(mix, this.intermediateTransform, null);
            buffid.g2dbi.drawImage(mix, this.posAtual.x, this.posAtual.y, null);    
            
//            System.out.println(this.posAtual.x + " = " + this.posAtual.y);
            buffid.repaint();
        }
        
        if ((int) alpha ==  1) {
            alpha = 0;
            this.rostoAtual++;
        }
            
        alpha = alpha + deltaAlpha;
                
        if (this.count < this.trajX.size())
            this.setPosAtual(new Ponto((int) this.trajX.get(this.count), (int) this.trajY.get(this.count)));
            
        this.count+= 1;
        
    }
    
    public static void main(String[] args) {
        Ponto aux;
        BufferedImage bi = new BufferedImage(1280, 1024, BufferedImage.TYPE_INT_RGB);
        
        BufferedImageDrawer bid = new BufferedImageDrawer(bi, 1280, 1024);
        
        QuadradoOito q = new QuadradoOito(bid);
        q.desenharCirculo(200, 200, 150);
        aux = new Ponto((int) q.trajX.get(0), (int) q.trajY.get(0));
        q.setPosAtual(aux);
 
        /*for (int i = 1; i < q.trajX.size(); i+= 16) {
            Ponto p = new Ponto((int) q.trajX.get(i), (int) q.trajY.get(i));
            q.moverPara(p);
        } */      
       
        Timer t = new Timer();
        t.scheduleAtFixedRate(q, 0, 30);
    }
}
